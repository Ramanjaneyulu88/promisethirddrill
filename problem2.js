/* 

  All rejected promises should return code and reason.
    { code: 400, reason: "BadRequest" }
    All Ids [cartItemId, userId, offerId] should be 10 digit random Number.
    All file operations should happen inside a promise. 


    products.json   // Contains all available products.
    cart.json       // Contains all selected cart items for a particular user
    offers.json     // Contains all the offers.

 Q.  Write a request to add Item to cart. The method should return a promise .

        The item passed to the function should be picked from products.json
        you should not end up removing the existing items.

        Signature: 
            should take a cartItem to add 
            cartItem could be 1 single or an array of items.

        Result: use the generated profile Id to add to cart.json. (use file write method)

        {
  '1249241498': [ { _id: '333434344394321344', name: 'Mobile Phone', quantity: 1 } ],
  '99299923488348': [ { _id: '33343402394321344', name: 'Umbrella', quantity: 4 } ],
  '992999234893920': [ { _id: '23829389283833838', name: 'Slippers', quantity: 2 } ]
}
*/
const fs = require("fs");
const path = require("path");

function logIn(username, type = 0) {
  let randomId = Math.floor(Math.random() * 10000000000);
  return new Promise((resolve, reject) => {
    if (username && username.length > 8) {
      reject({ code: 400, reason: "Invalid Username" });
    } else {
      resolve({ userId: randomId, userType: type });
    }
  });
}

function getProducts() {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, "products.json"), (err, data) => {
      if (err) {
        reject(err);
      } else {
        let productsArr = Object.entries(JSON.parse(data));
        // console.log(Object.entries(JSON.parse(data)));
        resolve(productsArr);
      }
    });
  });
}

getProducts().then(productsArr => {
    addItemToCart(productsArr).then(data => {
      console.log(data)
    })
})

function addItemToCart(productsArr) {
  return new Promise((resolve, reject) => {
    if (productsArr.length === 0) {
      reject("No item Selected");
    } else {
      // Login Details of User
      logIn("Rama", 0)
        .then((userObject) => {
            console.log(userObject)
          // Getting Cart Object 
          fs.readFile("./cart.json", (err, data) => {
            //console.log(JSON.parse(data.toString()));
            let cartObject = JSON.parse(data.toString());
            // Appending data to cart Object
            cartObject[userObject["userId"]] = [];
            
            let eachUserItems = cartObject[userObject["userId"]];
            for (let each of productsArr) {
              eachUserItems.push({
                _id: each[0],
                name: each[1]["name"],
                quanitty: 1,
              });
            }

           // console.log(cartObject)
            // Writing Final Output to cart Object
          
            fs.writeFile('cart.json',JSON.stringify(cartObject),(err) => {
          
            })
          });
        })
        .catch((err) => console.log(err));
      //fs.appendFile()
      resolve('Added to Cart');
    }
  });
}

//addItemToCart()
//let userObject = { userId: 6086677184, userType: 0 };
/*  {
  '1249241498': [ { _id: '333434344394321344', name: 'Mobile Phone', quantity: 1 } ],
  '99299923488348': [ { _id: '33343402394321344', name: 'Umbrella', quantity: 4 } ],
  '992999234893920': [ { _id: '23829389283833838', name: 'Slippers', quantity: 2 } ]
} */

/* let arr = [
  ["33343402394321344", { name: "Umbrella" }],
  ["33343402394321293", { name: "Television" }],
  ["23829389283833838", { name: "Slippers" }],
  ["333434344394321344", { name: "Mobile Phone" }],
  ["33343402394342293", { name: "Home Theatre" }],
  ["2382938928382349838", { name: "Hard Disk" }],
  ["33343402349091344", { name: "Vaccum Cleaner" }],
  ["32490320403294032432", { name: "Washing Machine" }],
  ["021930219309409343", { name: "Radio" }],
]; */


