/* Q.  Write a method to allow only admins users to add Item/Items to 
Catalog (products.json).
Signature - The argument passed could be an array of productItems or
 just a productItem
    "32490320403294032432": {                           
    "name": "Washing Machine"          
}

or 

[
    { 
        "32490320403294032432": {
        "name": "Washing Machine"
        }
    }
]


Method always appends data to products.json.
Once data is added to products.json.. it is mandatory to provide 
corresponding price details 
for those productIds (For price.json)

if Any required field in either of the data format is missing.
Throw an error 400, Bad request. */

const fs = require("fs");
const { resolve } = require("path");
const path = require("path");

function logIn(username, type = 0) {
  let randomId = Math.floor(Math.random() * 10000000000);
  return new Promise((resolve, reject) => {
    if (!username && username.length < 8 && type) {
      reject({ code: 400, reason: "Invalid Username Or Not Admin" });
    } else {
      resolve({ userId: randomId, userType: type });
    }
  });
}

function getProducts() {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, "products.json"), (err, data) => {
      if (err) {
        reject(err);
      } else {
        let productsArr = JSON.parse(data);
        // console.log(Object.entries(JSON.parse(data)));
        resolve(productsArr);
      }
    });
  });
}

function getPriceData() {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, "price.json"), (err, data) => {
      let priceData = JSON.parse(data.toString());
      //console.log(priceData)
      resolve(priceData);
    });
  });
}

let items = [
  {
    "32490320403294032432": {
      name: "Washing Machine",
    },
  },
  {
    "32490320403294032439": {
      name: "Air Cooler",
    },
  },
];
/* 
"33343402394321344": {
        "price": 339,
        "discount": 12,
        "applicableForOffers": false
    }, */

function updatePriceDetails(extraProducts) {
  return new Promise((resolve, reject) => {
    getPriceData()
      .then((priceData) => {
        //console.log(priceData)
        // Add new data to price data
        for (let each of extraProducts) {
          // console.log(each)
          priceData[Object.keys(each)[0]] = {
            price: Math.floor(Math.random() * 1000),
            discount: Math.floor(Math.random() * 100),
            applicableForOffers: Math.random() < 0.5,
          };

          // console.log(priceData)
        }

        // Update Price data

        fs.writeFile(
          path.join(__dirname, "price.json"),
          JSON.stringify(priceData),
          (err) => {
            if (err) throw err;
            console.log("Price Data Updated");
            resolve("Price data Updated");
          }
        );
      })
      .catch((err) => console.log(err));
  });
}

function addItemToCatalog(itemsToBeAdded) {
  return new Promise((resolve, reject) => {
    logIn("Ram", 1)
      .then((user) => {
        console.log(user);

        // Add Products to Product.

        getProducts().then((productData) => {
          itemsToBeAdded.forEach((eachItem) => {
            //console.log(eachItem);
            productData[Object.keys(eachItem)[0]] = {
              name: Object.values(eachItem)[0]["name"],
            };
            //console.log(productData);
          });

          console.log(productData);
          resolve("Products were added to Catalog");

          fs.writeFile(
            path.join(__dirname, "products.json"),
            JSON.stringify(productData),
            (err) => {
              if(err) throw err 
              updatePriceDetails(itemsToBeAdded)
            }
          );
        });
      })
      .catch((err) => {
        reject(err);
        //console.log(err)
      });
  });
}

addItemToCatalog(items)
  .then((data) => {
    console.log(data);
  })
  .catch((err) => console.log(err));
