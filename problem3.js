/* 
 Q.  Write a method to help user find the most expensive item with discount 
        and add that item to cart with quantity 1.

        price.json  
         "33343402394321344": {
        "price": 339,
        "discount": 12,
        "applicableForOffers": false
    },

*/

const fs = require("fs");
const { resolve } = require("path");
const path = require("path");
// LogIN Function
function logIn(username, type = 0) {
  let randomId = Math.floor(Math.random() * 10000000000);
  return new Promise((resolve, reject) => {
    if (username && username.length > 8) {
      reject({ code: 400, reason: "Invalid Username" });
    } else {
      resolve({ userId: randomId, userType: type });
    }
  });
}

/*  */

// Get Product Data Fuction

function getProductData() {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, "products.json"), (err, data) => {
      if (err) {
        console.log(err);
      } else {
        let productsData = JSON.parse(data.toString());
        // console.log(productsData);
        resolve(productsData);
      }
    });
  });
}

// Find Most Expensive

function findMostExpensive() {
  return new Promise((resolve, reject) => {
    // Accessing Price data
    fs.readFile(path.join(__dirname, "price.json"), (err, data) => {
      let priceData = JSON.parse(data.toString());
      //console.log(priceData);
      let highestPrice = 0;
      let mostExpenisveProduct = Object.entries(priceData).reduce(
        (acc, eachProduct) => {
          if (highestPrice < eachProduct[1]["price"]) {
            acc = {};
            acc[eachProduct[0]] = eachProduct[1];
            highestPrice = eachProduct[1]["price"];
          }

          return acc;
        },
        {}
      );

      // Access Product data to get name of Product

      getProductData().then((data) => {
        let productsData = data;
        // console.log(productsData)
        // Add name to most expensive
        Object.keys(productsData).forEach((eachKey) => {
          if (eachKey === Object.keys(mostExpenisveProduct)[0]) {
            //console.log(eachKey);
            let mostExpenisveProductDetails = {
              ...mostExpenisveProduct,

              name: productsData[eachKey]["name"],
            };
            // console.log(mostExpenisveProductDetails)
            resolve(mostExpenisveProductDetails);
          }
        });
      });
    });
  });
}

// let mostExpenisveProductDetails = {
//   '33343402394321293': { price: 889, discount: 33, applicableForOffers: true },
//   name: 'Television'
// }

function addToCart(expensiveProduct) {
  return new Promise((resolve, reject) => {
    // Getting User by Login
    logIn("Ram", 0).then((userObject) => {
      // console.log(userObject);
      // Getting Items in cart
      fs.readFile("./cart.json", (err, data) => {
        // console.log(JSON.parse(data.toString()));
        let cartObject = JSON.parse(data.toString());
        // Appending data to cart Object
        cartObject[userObject["userId"]] = [];
        cartObject[userObject["userId"]].push({
          _id: Object.keys(expensiveProduct)[0],
          name: expensiveProduct["name"],
          quantity: 1,
        });
        //console.log(cartObject);
        //  Writing Final Output to cart Object

        fs.writeFile("cart.json", JSON.stringify(cartObject), (err) => {
          resolve("Added to cart");
        });
      });
    });
  });
}

function findMostExpensiveAddToCart() {
  return new Promise((resolve, reject) => {
    findMostExpensive().then((mostExpenisveProduct) => {
      console.log(mostExpenisveProduct);
      addToCart(mostExpenisveProduct).then(data => {
        console.log(data)
        resolve('Expensive Product Added to Cart')
      })
    });
  });
}

//findMostExpensive().then(data => console.log(data))
//addToCart(mostExpenisveProductDetails);

findMostExpensiveAddToCart().then((data) => {console.log(data)});
