/*  Q.  Write a method to help user to extract all items with its 
name and price 
information to another file 
    with each information in a separate line so that he can pass
    that information to his friend .
 */

const fs = require("fs");
const { resolve } = require("path");
const path = require("path");
// Get product from  price.json
function getProductData() {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, "products.json"), (err, data) => {
      if (err) {
        console.log(err);
      } else {
        let productsData = JSON.parse(data.toString());
        // console.log(productsData);
        resolve(productsData);
      }
    });
  });
}

// Get Products Data from product.json




function getPriceData() {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, "price.json"), (err, data) => {
      let priceData = JSON.parse(data.toString());
      //console.log(priceData)
      resolve(priceData);
    });
  });
}

function extractItemsWithNameAndPrice() {
  return new Promise((resolve, reject) => {
    getProductData().then((productsData) => {
      getPriceData().then((priceData) => {
        //console.log(productsData);
        //console.log(priceData)
        // Iterate through Products
        let itesmWithNameAndPrice = {};
        for (let each in productsData) {
          //console.log(each);

          itesmWithNameAndPrice[productsData[each]['name']] = priceData[each]["price"];
        }

        resolve(itesmWithNameAndPrice)
       // console.log(itesmWithNameAndPrice)
      });
    });
  });
}

//getPriceData()

extractItemsWithNameAndPrice().then((data) => {console.log(data)})
