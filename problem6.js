/* 
 Q.  Write a request to add Item to cart. The method should return a promise .

        The item passed to the function should be picked from products.json
        you should not end up removing the existing items.

        Signature: 
            should take a cartItem to add 
            cartItem could be 1 single or an array of items.

        Result: use the generated profile Id to add to cart.json. (use file write method)

    Q.  Write a method to help user find the most expensive item with discount 
        and add that item to cart with quantity 1.

    Q.  Write a method to help user to extract all items with its name and price information to another file 
        with each information in a separate line so that he can pass that information to his friend .

    Q. Perform all the top operations for 1 more user */

const fs = require("fs");
const { resolve } = require("path");
const path = require("path");

function logIn(username, type = 0) {
  let randomId = Math.floor(Math.random() * 10000000000);
  return new Promise((resolve, reject) => {
    if (!username && username.length < 8) {
      reject({ code: 400, reason: "Invalid Username Or Not Admin" });
    } else {
      resolve({ userId: randomId, userType: type });
    }
  });
}

function getProducts() {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, "products.json"), (err, data) => {
      if (err) {
        reject(err);
      } else {
        let productsArr = JSON.parse(data);
        // console.log(Object.entries(JSON.parse(data)));
        resolve(productsArr);
      }
    });
  });
}

function getPriceData() {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, "price.json"), (err, data) => {
      let priceData = JSON.parse(data.toString());
      //console.log(priceData)
      resolve(priceData);
    });
  });
}

function findMostExpensive(productsData) {
  return new Promise((resolve, reject) => {
    // Accessing Price data
    fs.readFile(path.join(__dirname, "price.json"), (err, data) => {
      let priceData = JSON.parse(data.toString());
      //console.log(priceData);
      let highestPrice = 0;
      let mostExpenisveProduct = Object.entries(priceData).reduce(
        (acc, eachProduct) => {
          if (highestPrice < eachProduct[1]["price"]) {
            acc = {};
            acc[eachProduct[0]] = eachProduct[1];
            highestPrice = eachProduct[1]["price"];
          }

          return acc;
        },
        {}
      );

      // Access Product data to get name of Product

      // console.log(productsData)
      // Add name to most expensive
      Object.keys(productsData).forEach((eachKey) => {
        if (eachKey === Object.keys(mostExpenisveProduct)[0]) {
          //console.log(eachKey);
          let mostExpenisveProductDetails = {
            ...mostExpenisveProduct,

            name: productsData[eachKey]["name"],
          };
          // console.log(mostExpenisveProductDetails)
          resolve(mostExpenisveProductDetails);
        }
      });
    });
  });
}

function addToCartExpensive(expensiveProduct) {
  return new Promise((resolve, reject) => {
    // Getting User by Login
    logIn("Ram", 0).then((userObject) => {
      // console.log(userObject);
      // Getting Items in cart
      fs.readFile("./cart.json", (err, data) => {
        // console.log(JSON.parse(data.toString()));
        let cartObject = JSON.parse(data.toString());
        // Appending data to cart Object
        cartObject[userObject["userId"]] = [];
        cartObject[userObject["userId"]].push({
          _id: Object.keys(expensiveProduct)[0],
          name: expensiveProduct["name"],
          quantity: 1,
        });
        //console.log(cartObject);
        //  Writing Final Output to cart Object

        fs.writeFile("cart.json", JSON.stringify(cartObject), (err) => {
          resolve("Added to cart");
        });
      });
    });
  });
}

function addToCart(userObject, productsData) {
  return new Promise((resolve, reject) => {
    // Getting Cart Object
    fs.readFile("./cart.json", (err, data) => {
      // console.log(JSON.parse(data.toString()));
      let cartObject = JSON.parse(data.toString());
      let productsArr = Object.entries(productsData);
      // Appending data to cart Object
      cartObject[userObject["userId"]] = [];

      let eachUserItems = cartObject[userObject["userId"]];
      for (let each of productsArr) {
        eachUserItems.push({
          _id: each[0],
          name: each[1]["name"],
          quanitty: 1,
        });
      }

      // console.log(cartObject);
      // Writing Final Output to cart Object

      fs.writeFile("cart.json", JSON.stringify(cartObject), (err) => {
        if (err) {
          reject("Failed to add to cart");
        } else {
          resolve("Items Added to Cart");
        }
      });
    });
  });
}

function findMostExpensiveAddToCart(productsData) {
  return new Promise((resolve, reject) => {
    findMostExpensive(productsData).then((mostExpenisveProduct) => {
      console.log(mostExpenisveProduct);
      addToCartExpensive(mostExpenisveProduct).then((data) => {
        console.log(data);
        resolve("Expensive Product Added to Cart");
      });
    });
  });
}

function allOperations() {
  return new Promise((resolve, reject) => {
    // Loggin in
    logIn("Jaanu", 1)
      .then((userDetails) => {
        console.log("User Successfully logged In");
        // Accessing Products
        getProducts().then((productsData) => {
          //console.log(productsData);
          // Adding to cart
          addToCart(userDetails, productsData)
            .then((data) => {
              console.log("Data added To cart");
            })
            .catch((err) => console.log(err));
          // Adding the most expensive Item
          findMostExpensiveAddToCart(productsData).then((data) => {
            console.log(data);
          });
        });
        resolve('Done with shopping')
      })

      .catch((err) => {
        reject(err);
      });
  });
}

allOperations().then(data => {
  console.log(data)
})



