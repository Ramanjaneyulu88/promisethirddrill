/* 

    All rejected promises should return code and reason.
    { code: 400, reason: "BadRequest" }
    All Ids [cartItemId, userId, offerId] should be 10 digit random Number.
    All file operations should happen inside a promise. 


    products.json   // Contains all available products.
    cart.json       // Contains all selected cart items for a particular user
    offers.json     // Contains all the offers.


   { "_cartItemId": quantity }   // cartItem
    
    Q.  Create login function . Should return a promise.
        Signature: Takes 2 argument username, optional type argument with default value - 0 
        type - 1 (Admins)
        type - 0 (Normal/Guest user)
        Should return a 10 digit unique user id and a userType.
        Should throw an error if username is greater than 8 characters or is undefined. */

let users = {};
function logIn(username, type = 0) {
  let randomId = Math.floor(Math.random() * 10000000000);
  return new Promise((resolve, reject) => {
    if (username && username.length > 8) {
      reject({ code: 400, reason: "Invalid Username" });
    } else {
      resolve({ userId: randomId, userType: type });
    }
  });
}

logIn("Ramachari", 0)
  .then((data) => console.log(data))
  .catch((err) => console.log(err));
